import unittest
import dualbarcode


class test_themix(unittest.TestCase):

    def setUp(self):
        self.demul = dualbarcode.themix(fastq1="test_data/file1.fastq", fastq2="test_data/file2.fastq", nucode="test_data/bar1.txt", locuscode="test_data/bar2.txt", min_length=80, output_dir="test_data/output")
        #self.demul.fastq1 = "test_data/file1.fastq"
        #self.demul.fastq2 = "test_data/file2.fastq"
        #self.demul.min_len = 80
        #self.demul.nucode = "test_data/bar1.txt"
        #self.demul.locuscode = "test_data/bar2.txt"

    def tearDown(self):
        pass

    def test_read_fastq(self):
        reads = self.demul.read_fastq(self.demul.fastq1)
        self.assertEqual(len(reads), 8)

    def test_index_fastq(self):
        reads = self.demul.read_fastq(self.demul.fastq1)
        self.assertEqual(len(reads), 8)

    def test_if_barcode_file_is_not_empty(self):
        barcodes = self.demul.read_barcode_file(self.demul.nucode)
        self.assertEqual(barcodes.shape[0], 8)

    def test_that_barcode_file_is_tabular(self):
        barcodes = self.demul.read_barcode_file(self.demul.nucode)
        self.assertEqual(barcodes.shape[1], 2)

    def test_that_unify_barcodes_returns_dictionnary(self):
        unified = self.demul.unify_barcodes()
        self.assertEqual(len(unified), 64)

    def test_that_unify_barcodes_not_returning_the_exact_product(self):
        unified = self.demul.unify_barcodes()
        self.assertNotEqual(len(unified), 5)
