import argparse
from Bio import SeqIO
import pandas as pd
import itertools
import os


class themix(object):

    """
    docstring for Demultiplex
    """

    def __init__(self, fastq1, fastq2, nucode, locuscode, min_length, output_dir):

        self.fastq1 = fastq1
        self.fastq2 = fastq2
        self.nucode = nucode
        self.locuscode = locuscode
        self.min_length = min_length
        self.output_dir = output_dir

    def read_fastq(self, fastq_file):
        records = list(SeqIO.parse(fastq_file, "fastq"))
        return records

    def index_fastq(self, fastq_file):
        """
        Index the reverse read once for easy access without looping
        """
        records_dict = SeqIO.to_dict(SeqIO.parse(fastq_file, "fastq"))
        return records_dict

    def ListFiles(self, sPath, extension):
        # returns a list of names (with extension, with full path) of all files
        # in folder sPath
        lsFiles = []
        lsLabels = []
        for sName in os.listdir(sPath):
            if os.path.isfile(os.path.join(sPath, sName)) and sName.endswith(extension):
                lsFiles.append(os.path.join(sPath, sName))
                fileName, fileExtension = os.path.splitext(sName)
                sName = os.path.basename(fileName).split('.')[0]
                lsLabels.append(sName)
        return lsFiles, lsLabels

    def read_barcode_file(self, barcodefile):
        """
        reads barcode file
        """
        try:
            barcodes = pd.DataFrame()
            barcodes = pd.read_table(barcodefile, header=None)
            if barcodes.shape[1] > 2:
                barcodes = barcodes[barcodes.columns[0:2]]
            barcodes.columns = ['code_name', 'code_sequence']
            return barcodes
        except Exception, e:
            raise e

    def unify_barcodes(self):
        """
        Creates a product of all possible pairs of locus specific barcode and nuclear barcode
        """
        locus_barcodes = self.read_barcode_file(self.locuscode)
        nuclear_barcodes = self.read_barcode_file(self.nucode)
        locus_dict = locus_barcodes.set_index(
            'code_name')['code_sequence'].to_dict()
        nucode_dict = nuclear_barcodes.set_index(
            'code_name')['code_sequence'].to_dict()
        # Combine the barcodes into a single dictionnary
        unified_bc = {}
        unified_bc.update(locus_dict)
        unified_bc.update(nucode_dict)
        # extract ids as keys
        ids_locus = locus_barcodes['code_name'].tolist()
        ids_nuc = nuclear_barcodes['code_name'].tolist()
        # create the product dictionnary
        product_dict = {}
        for i, j in itertools.product(ids_locus, ids_nuc):
            product_dict.update({i + j: unified_bc[i] + unified_bc[j]})

        return product_dict

    def hamming_distance(self, s1, s2):
        """
        Returns the Hamming distance between equal-length sequences
        """
        if len(s1) != len(s2):
            raise ValueError("Undefined for sequences of unequal length")
        return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

    def demultiplex(self):
        """
        The function that is doing the big demultiplexing
        """
        candidates = self.unify_barcodes()
        forward = self.read_fastq(self.fastq1)
        reverse = self.index_fastq(self.fastq2)

        # we need a table to store the results for each sample
        # we need to create a directry called stats under the output diretory
        #
        stat_dir = os.path.join(self.output_dir, "stats")
        if not os.path.exists(stat_dir):
            os.makedirs(stat_dir)

        print "forward fastq contains %s reads" % len(forward)
        print "reverse fastq contains %s reads" % len(reverse)
        for record in forward:
            seq = "%s" % record.seq
            if len(seq) > int(self.min_length):
                seq_name = record.name
                segment = seq[0:12]
                score_list = []
                for barcode in candidates:
                    score = self.hamming_distance(segment, candidates[barcode])
                    # We are very strict here, we look at exact match
                    if score == 0:
                        original_file_name = os.path.basename(
                            self.fastq1).split(".")[0]
                        destination_file_name = barcode + \
                            "_" + original_file_name
                        destination_file_name = os.path.join(
                            self.output_dir, destination_file_name + ".fastq")
                        # I need to add trim here but will keep it for now
                        # because I am testing
                        record = record[12:]
                        with open(destination_file_name, "a") as forward_demult:
                            SeqIO.write(record, forward_demult, "fastq")
                        reverse_record = reverse[seq_name]
                        reverse_file_name = os.path.basename(
                            self.fastq2).split(".")[0]
                        destination_file_name_reverse = barcode + \
                            "_" + reverse_file_name
                        destination_file_name_reverse = os.path.join(
                            self.output_dir, destination_file_name_reverse + ".fastq")
                        with open(destination_file_name_reverse, "a") as reverse_demult:
                            SeqIO.write(
                                reverse_record, reverse_demult, "fastq")
                    else:
                        score_list.append(score)
                if len(score_list) == len(candidates):
                    with open(os.path.join(self.output_dir, "no_barcodes_found.fastq"), "a") as no_barcodes:
                        SeqIO.write(record, no_barcodes, "fastq")
            else:
                continue

    def report(self):
        demultiplexed_fastq, dualbarcode_labels = self.ListFiles(self.output_dir, ".fastq")
        candidates = self.unify_barcodes()

        #reads_per_barcode = []
        for demux_file, demux_label in zip(demultiplexed_fastq, dualbarcode_labels):
            demux_records = self.read_fastq(demux_file)
            number_of_reads = len(demux_records)
            #reads_per_barcode.append(number_of_reads)
            if demux_label[:4] in candidates.keys():
                print "%s  %s  %i" % (demux_label, candidates[demux_label[:4]], number_of_reads)
            else:
                print "%s      %i" % (demux_label, number_of_reads)


def main():
    """
    The purpose of this program is to do dual demultiplexing of fastq files before running a postprocessing step.
    There is also a filtering on the length of the read to be demultiplexed, if it is short,
    it is better to skip that read since it will not be used in the alignment further.
    The threshold for the length is subjective

    The programs takes as input : fastq forward and reverse files, text files

    Here is the logic:

    1- Read the locus specific barcodes => store in dictionnary
    2- Read the nucode specific barcodes => store in dictionnary
    3- unify dictionnaries of barcodes in a unique dictionnary
    4- create a new dictionnary of the unified barcode such as {key1key2:value1value2} example {'L1n6' : 'ACGTGCATCGATCGAGC'}
    5- open the forward read and start reading record by record:
       5.1 read the first 12bp of the read => segment
       5.2 compare the segment to the 64 unified dictionnary sequences
       5.3 store the results in a list
       5.4 sort the list, if all the 64 scores are > 0
                               dump the record in a fastq file called undefined
                          else if one score = 0
                               dump the record in a fastq file with a name like : unified_barcode_name + forward_filename
                               open the reverse read file
                               read record by record until we reach read name = current forward_read_name
                               dunmp that record into a file called : unified_barcode_name + reverse_filename
                          else
                               throw exemption "cannot be negative something is wrong"

    """

    parser = argparse.ArgumentParser(
        description='Demultiplexing dual (nested) barcodes ')

    parser.add_argument('--fq1',
                        help='''fastq file 1''')
    parser.add_argument('--fq2',
                        help='''fastq file 2''')
    parser.add_argument('--nuc',
                        help='''Nuclear Barcode File''')
    parser.add_argument('--loc',
                        help='''Locus specific barcode''')
    parser.add_argument('--min_len',
                        help='''minimum length to process a read''')
    parser.add_argument('--output_dir',
                        help='''path to the output directory''')
    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    mix = themix(fastq1=args.fq1, fastq2=args.fq2, nucode=args.nuc,
                 locuscode=args.loc, min_length=args.min_len, output_dir=args.output_dir)

    #print mix.unify_barcodes()
    mix.demultiplex()
    mix.report()

if __name__ == '__main__':
    main()
